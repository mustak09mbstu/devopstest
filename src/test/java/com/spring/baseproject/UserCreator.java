package com.spring.baseproject;

import com.spring.baseproject.entities.Authority;
import com.spring.baseproject.entities.User;
import com.spring.baseproject.enumeration.Role;
import com.spring.baseproject.repositories.AuthorityRepository;
import com.spring.baseproject.repositories.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class UserCreator {
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private AuthorityRepository authorityRepository;

	@Test
	public void createUser(){
		User user = new User();
		user.setUsername("admin");
		user.setFullName("admin");
		user.setFullNameBangla("admin");
		user.setPlainPassword("admin");
		user.setEnabled(true);
		userRepository.save(user);
		
		Authority authority = new Authority();
		authority.setAuthority(Role.ROLE_ADMINISTRATOR.toString());
		authority.setUsername("admin");
		authorityRepository.save(authority);
		
	}
}
