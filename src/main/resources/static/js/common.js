/**
 * Created by rana on 11/26/16.
 */
function fullWidthNotification(msg, type) {
    if (typeof type == 'undefined') {
        type = 'success';
    }
    toastr.options = {
        "closeButton": true,
        "progressBar": true,
        "positionClass": "toast-top-full-width",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };

    toastr[type](msg);
}

function reloadPage(delay) {
    setTimeout('location.reload(true);', delay);
}

function blockUI() {
    $.blockUI({ message: '<h1> Just wait a moment<img style="width: 385px;" src="/img/processing.gif" /></h1>' });
}
function unBlockUI() {
    $.unblockUI();
}

function redirectPage(url, delay) {
    setTimeout(function(){location.replace(url);}, delay);
}

function initDatePicker($el, option) {

    if (jQuery().datepicker) {
        var defaultOption = {
            orientation: "left",
            autoclose: true,
            format: 'dd/mm/yyyy'
        };

        $el.datepicker($.extend(defaultOption, (option || {})));

        $el.datepicker().on('hide.bs.modal', function(event) {
            event.stopPropagation();
        });
    }
}

initDatePicker($('.date-picker'));