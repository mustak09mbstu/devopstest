package com.spring.baseproject.entities;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import org.dom4j.tree.AbstractEntity;

import java.util.Date;

@Entity
@Table(name = "EMPLOYEES")
@XmlRootElement
public class Employees extends AbstractEntity {

	@SequenceGenerator(name = "EMPLOYEES_SEQUENCE_GENERATOR", sequenceName = "EMPLOYEES_ID_SEQ")
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "EMPLOYEES_SEQUENCE_GENERATOR")
	@Basic(optional = false)
	@Column(name = "ID", nullable = false, precision = 38, scale = 0)
	private long id;

	@Column(name = "EMPLOYEE_ID", length = 50)
	private String employee_id;

	@Column(name = "FIRST_NAME", length = 50)
	private String first_name;

	@Column(name = "MIDDLE_NAME", length = 50)
	private String middle_name;

	@Column(name = "LAST_NAME", length = 50)
	private String last_name;

	@ManyToOne
	@JoinColumn(name = "NATIONALITY")
	private Nationality nationality;

	@Column(name = "BIRTHDAY")
	private Date birthday;

	@Column(name = "GENDER", length = 10)
	private String gender;

	@Column(name = "MARITAL_STATUS", length = 20)
	private String marital_status;

	@Column(name = "SSN_NUM", length = 100)
	private String ssn_num;

	@Column(name = "NIC_NUM", length = 100)
	private String nic_num;

	@Column(name = "OTHER_ID", length = 100)
	private String other_id;

	@Column(name = "DRIVING_LICENSE", length = 100)
	private String driving_license;

	@Column(name = "DRIVING_LICENSE_EXP_DATE")
	private Date driving_license_exp_date;

	@Column(name = "WORK_STATUION_ID", length = 100)
	private String work_station_id;

	@Column(name = "ADDRESS1", length = 100)
	private String address1;

	@Column(name = "ADDRESS2", length = 100)
	private String address2;

	@Column(name = "CITY", length = 50)
	private String city;

	@Column(name = "POSTAL_CODE", length = 20)
	private String postal_code;

	@Column(name = "HOME_PHONE", length = 50)
	private String home_phone;

	@Column(name = "MOBILE_PHONE", length = 50)
	private String mobile_phone;

	@Column(name = "WORK_PHONE", length = 50)
	private String work_phone;

	@Column(name = "WORK_EMAIL", length = 50)
	private String work_email;

	@Column(name = "PRIVATE_EMAIL", length = 50)
	private String private_email;

	@Column(name = "JOINED_DATE")
	private Date joined_date;

	@Column(name = "CONFIRMATION_DATE")
	private Date confirmation_date;

	@ManyToOne
	@JoinColumn(name = "SUPERVISOR_Id")
	private Employees supervisor;

	@Column(name = "INDIRECT_SUPPERVISORS", length = 250)
	private String indirect_supervisors;

	@Column(name = "CUSTOM1", length = 250)
	private String custom1;

	@Column(name = "CUSTOM2", length = 250)
	private String custom2;

	@Column(name = "CUSTOM3", length = 250)
	private String custom3;

	@Column(name = "CUSTOM4", length = 250)
	private String custom4;

	@Column(name = "CUSTOM5", length = 250)
	private String custom5;

	@Column(name = "CUSTOM6", length = 250)
	private String custom6;

	@Column(name = "CUSTOM7", length = 250)
	private String custom7;

	@Column(name = "CUSTOM8", length = 250)
	private String custom8;

	@Column(name = "CUSTOM9", length = 250)
	private String custom9;

	@Column(name = "CUSTOM10", length = 250)
	private String custom10;

	@Column(name = "TERMINATION_DATE")
	private Date termination_date;

	@Column(name = "NOTES", length = 500)
	private String notes;

	@Column(name = "STATUS", length = 10)
	private String status;

	@Column(name = "ETHNICITY")
	private long ethnicity;

	@Column(name = "IMMIGRATION_STATUS")
	private long immigration_status;

	@Column(name = "APPROVER1")
	private long approver1;

	@Column(name = "APPROVER2")
	private long approver2;

	@Column(name = "APPROVER3")
	private long approver3;

	@Column(name = "PAY_GRADE_START_DATE")
	private Date payGradeStartDate;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getEmployee_id() {
		return employee_id;
	}

	public void setEmployee_id(String employee_id) {
		this.employee_id = employee_id;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getMiddle_name() {
		return middle_name;
	}

	public void setMiddle_name(String middle_name) {
		this.middle_name = middle_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public Nationality getNationality() {
		return nationality;
	}

	public void setNationality(Nationality nationality) {
		this.nationality = nationality;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getMarital_status() {
		return marital_status;
	}

	public void setMarital_status(String marital_status) {
		this.marital_status = marital_status;
	}

	public String getSsn_num() {
		return ssn_num;
	}

	public void setSsn_num(String ssn_num) {
		this.ssn_num = ssn_num;
	}

	public String getNic_num() {
		return nic_num;
	}

	public void setNic_num(String nic_num) {
		this.nic_num = nic_num;
	}

	public String getOther_id() {
		return other_id;
	}

	public void setOther_id(String other_id) {
		this.other_id = other_id;
	}

	public String getDriving_license() {
		return driving_license;
	}

	public void setDriving_license(String driving_license) {
		this.driving_license = driving_license;
	}

	public Date getDriving_license_exp_date() {
		return driving_license_exp_date;
	}

	public void setDriving_license_exp_date(Date driving_license_exp_date) {
		this.driving_license_exp_date = driving_license_exp_date;
	}

	public String getWork_station_id() {
		return work_station_id;
	}

	public void setWork_station_id(String work_station_id) {
		this.work_station_id = work_station_id;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPostal_code() {
		return postal_code;
	}

	public void setPostal_code(String postal_code) {
		this.postal_code = postal_code;
	}

	public String getHome_phone() {
		return home_phone;
	}

	public void setHome_phone(String home_phone) {
		this.home_phone = home_phone;
	}

	public String getMobile_phone() {
		return mobile_phone;
	}

	public void setMobile_phone(String mobile_phone) {
		this.mobile_phone = mobile_phone;
	}

	public String getWork_phone() {
		return work_phone;
	}

	public void setWork_phone(String work_phone) {
		this.work_phone = work_phone;
	}

	public String getWork_email() {
		return work_email;
	}

	public void setWork_email(String work_email) {
		this.work_email = work_email;
	}

	public String getPrivate_email() {
		return private_email;
	}

	public void setPrivate_email(String private_email) {
		this.private_email = private_email;
	}

	public Date getJoined_date() {
		return joined_date;
	}

	public void setJoined_date(Date joined_date) {
		this.joined_date = joined_date;
	}

	public Date getConfirmation_date() {
		return confirmation_date;
	}

	public void setConfirmation_date(Date confirmation_date) {
		this.confirmation_date = confirmation_date;
	}

	public Employees getSupervisor() {
		return supervisor;
	}

	public void setSupervisor(Employees supervisor) {
		this.supervisor = supervisor;
	}

	public String getIndirect_supervisors() {
		return indirect_supervisors;
	}

	public void setIndirect_supervisors(String indirect_supervisors) {
		this.indirect_supervisors = indirect_supervisors;
	}

	public String getCustom1() {
		return custom1;
	}

	public void setCustom1(String custom1) {
		this.custom1 = custom1;
	}

	public String getCustom2() {
		return custom2;
	}

	public void setCustom2(String custom2) {
		this.custom2 = custom2;
	}

	public String getCustom3() {
		return custom3;
	}

	public void setCustom3(String custom3) {
		this.custom3 = custom3;
	}

	public String getCustom4() {
		return custom4;
	}

	public void setCustom4(String custom4) {
		this.custom4 = custom4;
	}

	public String getCustom5() {
		return custom5;
	}

	public void setCustom5(String custom5) {
		this.custom5 = custom5;
	}

	public String getCustom6() {
		return custom6;
	}

	public void setCustom6(String custom6) {
		this.custom6 = custom6;
	}

	public String getCustom7() {
		return custom7;
	}

	public void setCustom7(String custom7) {
		this.custom7 = custom7;
	}

	public String getCustom8() {
		return custom8;
	}

	public void setCustom8(String custom8) {
		this.custom8 = custom8;
	}

	public String getCustom9() {
		return custom9;
	}

	public void setCustom9(String custom9) {
		this.custom9 = custom9;
	}

	public String getCustom10() {
		return custom10;
	}

	public void setCustom10(String custom10) {
		this.custom10 = custom10;
	}

	public Date getTermination_date() {
		return termination_date;
	}

	public void setTermination_date(Date termination_date) {
		this.termination_date = termination_date;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public long getEthnicity() {
		return ethnicity;
	}

	public void setEthnicity(long ethnicity) {
		this.ethnicity = ethnicity;
	}

	public long getImmigration_status() {
		return immigration_status;
	}

	public void setImmigration_status(long immigration_status) {
		this.immigration_status = immigration_status;
	}

	public long getApprover1() {
		return approver1;
	}

	public void setApprover1(long approver1) {
		this.approver1 = approver1;
	}

	public long getApprover2() {
		return approver2;
	}

	public void setApprover2(long approver2) {
		this.approver2 = approver2;
	}

	public long getApprover3() {
		return approver3;
	}

	public void setApprover3(long approver3) {
		this.approver3 = approver3;
	}

	public Date getPayGradeStartDate() {
		return payGradeStartDate;
	}

	public void setPayGradeStartDate(Date payGradeStartDate) {
		this.payGradeStartDate = payGradeStartDate;
	}

}
