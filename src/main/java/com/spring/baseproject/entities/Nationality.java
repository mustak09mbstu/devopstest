package com.spring.baseproject.entities;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import org.dom4j.tree.AbstractEntity;

@Entity
@Table(name = "NATIONALITY")
@XmlRootElement
public class Nationality extends AbstractEntity {

	@SequenceGenerator(name = "NATIONALITY_SEQUENCE_GENERATOR", sequenceName = "NATIONALITY_ID_SEQUENCE")
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "NATIONALITY_SEQUENCE_GENERATOR")
	@Basic(optional = false)
	@Column(name = "ID", nullable = false, precision = 38, scale = 0)
	private Integer id;

	@Column(name = "NAME", length = 50)
	private String name;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
