package com.spring.baseproject.repositories;

import com.spring.baseproject.entities.User;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface UserRepository extends PagingAndSortingRepository<User, Integer> {
	
}
