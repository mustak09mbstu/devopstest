package com.spring.baseproject.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.spring.baseproject.entities.Nationality;

public interface NationalityRepository extends PagingAndSortingAndSpecificationExecutorRepository<Nationality>{

	@Query("SELECT x FROM Nationality x WHERE x.id = :id ")
    public Nationality findById(@Param("id") Integer id);
}
