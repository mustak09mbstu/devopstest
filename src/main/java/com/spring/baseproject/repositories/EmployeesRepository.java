package com.spring.baseproject.repositories;

import com.spring.baseproject.entities.Employees;

public interface EmployeesRepository extends PagingAndSortingAndSpecificationExecutorRepository<Employees> {
	
}
