package com.spring.baseproject.controllers;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import com.spring.baseproject.entities.Employees;
import com.spring.baseproject.entities.Nationality;
import com.spring.baseproject.repositories.EmployeesRepository;
import com.spring.baseproject.repositories.NationalityRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


@Controller
@RequestMapping("/employee/")
public class EmployeeController {

	@Autowired
	private EmployeesRepository employeesRepository;
	@Autowired
	private NationalityRepository nationalityRepository;
	
	@RequestMapping(value = "/form", method = RequestMethod.GET)
	public ModelAndView createForm(@ModelAttribute Employees employees, ModelMap map) {
		map.put("employeesDropDown", (List<Employees>)employeesRepository.findAll() );
		map.put("nationalitys", (List<Nationality>)nationalityRepository.findAll() );
		return new ModelAndView("/employee/form",map);
	}

	@RequestMapping
	public ModelAndView list() {

		return paginationList(1);
	}

	@RequestMapping(value = "/page/{pageNumber}", method = RequestMethod.GET)
	public ModelAndView paginationList(@PathVariable Integer pageNumber) {

		Map<String, Object> model = new HashMap<String, Object>();
		PageRequest pageRequest = new PageRequest(pageNumber - 1, 20);
		Page<Employees> currentResults = employeesRepository.findAll(pageRequest);

		model.put("employees", currentResults);
		// Pagination variables
		int current = currentResults.getNumber() + 1;
		int begin = Math.max(1, current - 5);
		int end = Math.min(begin + 10, currentResults.getTotalPages());

		model.put("beginIndex", begin);
		model.put("endIndex", end);
		model.put("currentIndex", current);

		return new ModelAndView("employee/list", model);
	}

	/*------------------------------------------------------------------Edit-----------------------------*/

	@RequestMapping(value = "edit/{id}", method = RequestMethod.GET)
	public ModelAndView editForm(@PathVariable("id") Long id) {
		Employees employees = employeesRepository.findOne(id);
		return new ModelAndView("/employee/form", "employees", employees);

	}

	/* .......................delete................ */

	@RequestMapping(value = "delete/{id}", method = RequestMethod.GET)
	public ModelAndView deleteForm(@PathVariable("id") Long id, RedirectAttributes redirect) {
		Employees employees = employeesRepository.findOne(id);
		employeesRepository.delete(employees);
		redirect.addFlashAttribute("globalMessage", "Successfully Deleted the author");
		return new ModelAndView("redirect:/employee/");

	}

	/*------------------------------------------------------------------View-----------------------------*/

	@RequestMapping(value = "view/{id}", method = RequestMethod.GET)
	public ModelAndView viewForm(@PathVariable("id") Long id) {
		Employees employees = employeesRepository.findOne(id);
		return new ModelAndView("/employee/view", "employee", employees);

	}

	/*------------------------------------------------------------------Save-----------------------------*/

	@RequestMapping(value = {"form","edit/form"}, method = RequestMethod.POST)
	public ModelAndView save(@Valid Employees employees, BindingResult result, RedirectAttributes redirect) {

		String message = "";

		if (employees.getId() == 0) {
			message = "Successfully new entry added";
			System.out.println("Successfully new entry added");
		} else {
			message = "Successfully edited";
			System.out.println("Successfully edited");
		}

		employeesRepository.save(employees);

		redirect.addFlashAttribute("globalMessage", message);
		return new ModelAndView("redirect:/employee/");
	}
}
