package com.spring.baseproject.controllers;

import com.spring.baseproject.entities.Nationality;
import com.spring.baseproject.repositories.NationalityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/nationality/")
public class NationalityController {

	@Autowired
	private NationalityRepository nationalityRepository;

	@RequestMapping(value = "/form", method = RequestMethod.GET)
	public String createForm(@ModelAttribute Nationality nationality) {
		return "/nationality/form";
	}

	@RequestMapping
	public ModelAndView list() {

		return paginationList(1);
	}

	@RequestMapping(value = "/page/{pageNumber}", method = RequestMethod.GET)
	public ModelAndView paginationList(@PathVariable Integer pageNumber) {

		Map<String, Object> model = new HashMap<String, Object>();
		PageRequest pageRequest = new PageRequest(pageNumber - 1, 20);
		Page<Nationality> currentResults = nationalityRepository.findAll(pageRequest);

		model.put("nationality", currentResults);
		// Pagination variables
		int current = currentResults.getNumber() + 1;
		int begin = Math.max(1, current - 5);
		int end = Math.min(begin + 10, currentResults.getTotalPages());

		model.put("beginIndex", begin);
		model.put("endIndex", end);
		model.put("currentIndex", current);

		return new ModelAndView("nationality/list", model);
	}

	/*------------------------------------------------------------------Edit-----------------------------*/

	@RequestMapping(value = "edit/{id}", method = RequestMethod.GET)
	public ModelAndView editForm(@PathVariable("id") Integer id, @ModelAttribute Nationality nationality) {
		nationality = nationalityRepository.findById(id);
		return new ModelAndView("/nationality/form", "nationality", nationality);

	}

	/* .......................delete................ */

	@RequestMapping(value = "delete/{id}", method = RequestMethod.GET)
	public ModelAndView deleteForm(@PathVariable("id") Integer id, RedirectAttributes redirect) {
		Nationality nationality = nationalityRepository.findById(id);
		nationalityRepository.delete(nationality);
		redirect.addFlashAttribute("globalMessage", "Successfully Deleted the author");
		return new ModelAndView("redirect:/nationality/");

	}
	/*------------------------------------------------------------------View-----------------------------*/

	@RequestMapping(value = "view/{id}", method = RequestMethod.GET)
	public ModelAndView viewForm(@PathVariable("id") Integer id) {
		Nationality nationality = nationalityRepository.findById(id);
		return new ModelAndView("/nationality/view", "nationality", nationality);

	}

	/*------------------------------------------------------------------Save-----------------------------*/

	@RequestMapping(value = {"form","edit/form"}, method = RequestMethod.POST)
	public ModelAndView save(@Valid Nationality nationality, BindingResult result, RedirectAttributes redirect) {

		String message = "";

		if (null == nationality.getId()  ) {
			message = "Successfully new entry added";
			System.out.println("Successfully new entry added");
		} else {
			message = "Successfully edited";
			System.out.println("Successfully edited");
		}

		nationalityRepository.save(nationality);

		redirect.addFlashAttribute("globalMessage", message);
		return new ModelAndView("redirect:/nationality/");
	}

}
